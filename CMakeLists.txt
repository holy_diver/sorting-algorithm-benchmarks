cmake_minimum_required(VERSION 3.16)
project(sorting-algorith-benchmarks C)

set(CMAKE_C_STANDARD 11)

add_executable(main.exe main.c)