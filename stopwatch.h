#include <time.h>

clock_t startTime, endTime;

void start()
{
    startTime = clock();
}

double stop()
{
    endTime = clock();
    return ((double)(endTime - startTime)) / CLOCKS_PER_SEC;
}