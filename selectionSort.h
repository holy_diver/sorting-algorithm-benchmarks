void selectionSort(int array[], int length)
{
    int i, j, minIndex, temp;
    for (i = 0; i < length; i++)
    {
        minIndex = i;
        for (j = i + 1; j < length; j++)
        {
            if (array[minIndex] > array[j])
                minIndex = j;
        }
        temp = array[i];
        array[i] = array[minIndex];
        array[minIndex] = temp;
    }
}