void insertionSort(int array[], int length)
{
    int i, j, temp;
    for (i = 1; i < length; i++)
    {
        j = i - 1;
        temp = array[i];
        while (j >= 0 && array[j] > temp)
        {
            array[j + 1] = array[j];
            j -= 1;
        }
        array[j + 1] = temp;
    }
}