#include <stdlib.h>

void fillPartlySorted(int array[], int length, int sortAmount)
{
    int i, temp;
    for (i = 0; i < length; i++)
    {
        if (i % (sortAmount + 1) == 0)
        {
            temp = (rand() % (length * 3)) % temp;
            array[i] = temp;
            temp = rand() % (length * 3);
        }
        else
        {
            temp++;
            array[i] = temp;
        }
    }
}

void fillUnsorted(int array[], int length)
{
    int i;
    for (i = 0; i < length; i++)
    {
        array[i] = rand() % (length * 3);
    }
}