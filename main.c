#include <stdio.h>
#include <stdlib.h>
#include "insertionSort.h"
#include "selectionSort.h"
#include "bubbleSort.h"
#include "stopwatch.h"
#include "arrayGenerator.h"
#define N 90000
#define partlySortedArraySortAmount 1000

void printArray(int array[], int length);
double benchInsertionSort(int array[], int length);
double benchSelectionSort(int array[], int length);
double benchBubbleSort(int array[], int length);

int main()
{
    printf("This program will run 5 sorting algorithms for both partly sorted and virtually unsorted arrays, 3 times for each, and show average time each one takes for a/an %d sized array", N);
    printf("\n");
    printf("Algorithms will sort with increasing order");
    printf("\n");

    double avgTime = 0;
    int i, array[N];
    //insertionSort, unsorted
    for (i = 0; i < 3; i++)
    {
        fillUnsorted(array, N);
        avgTime += benchInsertionSort(array, N);
    }
    avgTime /= 3;
    printf("\n");
    printf("Insertion for unsorted array, AvgTime = %lf seconds", avgTime);
    printf("\n");
    avgTime = 0;

    //selectionSort, unsorted
    for (i = 0; i < 3; i++)
    {
        fillUnsorted(array, N);
        avgTime += benchSelectionSort(array, N);
    }
    avgTime /= 3;
    printf("\n");
    printf("Selection for unsorted array, AvgTime = %lf seconds", avgTime);
    printf("\n");
    avgTime = 0;

    //bubbleSort, unsorted
    for (i = 0; i < 3; i++)
    {
        fillUnsorted(array, N);
        avgTime += benchBubbleSort(array, N);
    }
    avgTime /= 3;
    printf("\n");
    printf("Bubble for unsorted array, AvgTime    = %lf seconds", avgTime);
    printf("\n");
    avgTime = 0;

    printf("\n");
    printf("For partly sorted arrays, sort amount = %d, meaning there will be a random number per %d index, guaranteed to be smaller than the previous one:", partlySortedArraySortAmount, partlySortedArraySortAmount);
    printf("\n");

    //insertionSort, partly sorted
    for (i = 0; i < 3; i++)
    {
        fillPartlySorted(array, N, partlySortedArraySortAmount);
        avgTime += benchInsertionSort(array, N);
    }
    avgTime /= 3;
    printf("\n");
    printf("Insertion for partly sorted array, AvgTime = %lf seconds", avgTime);
    printf("\n");
    avgTime = 0;

    //selectionSort, partly sorted
    for (i = 0; i < 3; i++)
    {
        fillPartlySorted(array, N, partlySortedArraySortAmount);
        avgTime += benchSelectionSort(array, N);
    }
    avgTime /= 3;
    printf("\n");
    printf("Selection for partly sorted array, AvgTime = %lf seconds", avgTime);
    printf("\n");
    avgTime = 0;

    //bubbleSort, partly sorted
    for (i = 0; i < 3; i++)
    {
        fillPartlySorted(array, N, partlySortedArraySortAmount);
        avgTime += benchBubbleSort(array, N);
    }
    avgTime /= 3;
    printf("\n");
    printf("Bubble for partly sorted array, AvgTime    = %lf seconds", avgTime);
    printf("\n");
    avgTime = 0;
    getchar();
}

void printArray(int array[], int length)
{
    int i;
    printf("{");
    for (i = 0; i < length; i++)
    {
        printf("%d", array[i]);
        if (i < length - 1)
            printf(", ");
    }
    printf("}");
}

double benchInsertionSort(int array[], int length)
{
    double timePassed;
    start();
    insertionSort(array, length);
    timePassed = stop();
    printf("\n");
    printf("Insertion Sort - Time passed: %lf seconds.", timePassed);
    printf("\n");
    return timePassed;
}

double benchSelectionSort(int array[], int length)
{
    double timePassed;
    start();
    selectionSort(array, length);
    timePassed = stop();
    printf("\n");
    printf("Selection Sort - Time passed: %lf seconds.", timePassed);
    printf("\n");
    return timePassed;
}

double benchBubbleSort(int array[], int length)
{
    double timePassed;
    start();
    bubbleSort(array, length);
    timePassed = stop();
    printf("\n");
    printf("Bubble Sort - Time passed: %lf seconds.", timePassed);
    printf("\n");
    return timePassed;
}